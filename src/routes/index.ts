import { Router } from "express";
import { addressRoutes } from "./addresses.routes";
import { basketRoutes } from "./basket.routes";
import { paymentsRoutes } from "./payments.routes";
import { reviewRoutes } from "./reviews.routes";
import { userRoutes } from "./users.routes";
import { wishListRoutes } from "./wishlist.routes";

const routes = Router()
    .use('/users', userRoutes)
    .use('/basket', basketRoutes)
    .use('/review', reviewRoutes)
    .use('/address', addressRoutes)
    .use('/payments', paymentsRoutes)
    .use('/wishlist', wishListRoutes)

export default routes;